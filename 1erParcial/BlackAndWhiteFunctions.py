from pip._vendor.distlib.compat import raw_input
from skimage import data, io, util
from Pymages import BlackAndWhite as bnw
import matplotlib.pyplot as plt

img = io.imread('background.png')
image = util.img_as_ubyte(img)
io.imshow(img)
imgCollection = io.image_stack;
io.show()
while True:
    selection=raw_input("Please select your function: ")
    if selection == '1':
        res = bnw.redchannel(image)
    elif selection == '2':
        res = bnw.greenchannel(image)
    elif selection == '3':
        res = bnw.bluechannel(image)
    elif selection == '4':
        res = bnw.greyshadow(image)
    elif selection == '5':
        res = bnw.desaturation(image)
    elif selection == '6':
        res = bnw.maxdecomposition(image)
    elif selection == '7':
        res = bnw.mindecomposition(image)
    elif selection == '8':
        res = bnw.avarage(image)
    elif selection == '9':
        res = bnw.lumaPhotoshop(image)
    elif selection == '10':
        res = bnw.lumaBT709(image)
    elif selection == '11':
        res = bnw.lumaBT601(image)
    elif selection == '12':
        break
    else:
        print("Unkwon Option Selected")
    io.imshow(res)
    io.show()
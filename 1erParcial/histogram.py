from pip._vendor.distlib.compat import raw_input
from skimage import data, io, util
from Pymages import BlackAndWhite as bnw
from Pymages import Hist as hst

imge = io.imread('background.png')
img = bnw.bluechannel(imge)
io.imshow(img)
io.show()
#vector = hst.histogram(img)
vector2 = hst.histogramacumulated(img)
from pip._vendor.distlib.compat import raw_input
from skimage import data, io, util
from Pymages import AffineTransformations as at
from Pymages import BlackAndWhite as bnw
import matplotlib.pyplot as plt

imge = io.imread('background.png')
img = bnw.redchannel(imge)
io.imshow(img)
imgCollection = io.image_stack;
io.show()
selection = raw_input("Please select your function: ")
if selection == '1':
    res = at.scale(img, 0.5, 0.5)
elif selection == '2':
    res = at.rotation(img, 90)
elif selection == '3':
    res = at.translation(img, 200, 200)
elif selection == '4':
    res = at.shearh(img, 0.3)
elif selection == '5':
    res = at.shearv(img, 0.3)
else:
    res = img

io.imshow(res)
io.show()
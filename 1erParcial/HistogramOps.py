from pip._vendor.distlib.compat import raw_input
from skimage import data, io, util
from Pymages import BlackAndWhite as bnw
from Pymages import Calculations as cal

imge = io.imread('background.png')
img = bnw.greyshadow(imge)
io.imshow(img)
io.show()
selection = raw_input("Please select your function: ")
if selection == '1':
    res = cal.illumination(img)
    print(res)
elif selection == '2':
    res = cal.contrast(img)
    print(res)
elif selection == '3':
    res = cal.dynamic(img)
    print(res)
elif selection == '4':
    res = cal.equalization(img)
    print(res)
elif selection == '5':
    res = cal.adaptation(img)
    io.imshow(res)
    io.show()

from pip._vendor.distlib.compat import raw_input
from skimage import data, io, util
from Pymages import PixelOpers as pops
from Pymages import BlackAndWhite as bnw
import matplotlib.pyplot as plt

imge = io.imread('background.png')
img = bnw.redchannel(imge)
io.imshow(img)
imgCollection = io.image_stack;
io.show()
selection = raw_input("Please select your function: ")
if selection == '1':
    #complemento
    res = pops.complement(img)
elif selection == '2':
    #contraste mult
    res = pops.contrast(img, 1.5)
elif selection == '3':
    #brillo suma
    res = pops.brilliance(img, 50)
else:
    res = img

io.imshow(res)
io.show()
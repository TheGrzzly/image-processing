from pip._vendor.distlib.compat import raw_input
from skimage import data, io, util
from skimage.filters import threshold_otsu
from Pymages import BooleanOps as bops
from Pymages import BlackAndWhite as bnw
import numpy

imge = io.imread('thonking.jpg')
img = bnw.avarage(imge)
imge2 = util.random_noise(imge, 's&p')
img2 = bnw.avarage(imge2)
# io.imshow(img2)
# io.show()
thres2 = threshold_otsu(img2)
thresh = threshold_otsu(img)
binary_noise = img2 > thres2
binary = img > thresh
io.imshow(binary)
io.show()
dilate = numpy.array([
    [False, False, False, False, False, False, False, False, False, False],
    [False, False, False, False, False, False, False, False, False, False],
    [False, False, False, False, False, False, True, False, False, False],
    [False, False, False, False, True, True, True, True, False, False],
    [False, False, False, True, True, True, True, True, True, False],
    [False, False, True, True, True, True, True, True, True, False],
    [False, True, True, True, True, True, True, True, True, False],
    [False, False, False, False, True, False, True, True, False, False],
    [False, False, False, False, False, False, False, True, False, False],
    [False, False, False, False, False, False, False, False, False, False]
])
io.imshow(dilate)
io.show()
erode = numpy.array([
    [False, False, False, False, False, False, False, False, False, False],
    [False, False, False, False, False, False, False, False, False, False],
    [False, False, False, False, False, False, False, False, False, False],
    [False, False, False, True, True, False, True, False, False, False],
    [False, False, False, True, True, True, True, True, False, False],
    [False, False, True, True, True, True, True, False, False, False],
    [False, False, False, True, True, False, True, True, False, False],
    [False, False, False, True, False, False, False, False, False, False],
    [False, False, False, False, False, False, False, False, False, False],
    [False, False, False, False, False, False, False, False, False, False]
])
io.imshow(erode)
io.show()
reference = numpy.array([
    [False, True, False],
    [True, True, True],
    [False, True, False]
])
io.imshow(reference)
io.show()
selection = raw_input("Please select your function: ")
if selection == '1':
    res = bops.dilate(erode, reference)
    print("dilatation complete")
elif selection == '2':
    res = bops.erode(dilate, reference)
    print("erosion complete")
elif selection == '3':
    res = bops.lock(binary, reference)
    print("closing complete")
elif selection == '4':
    res = bops.opening(binary, reference)
    print("open complete")
elif selection == '5':
    res = bops.eliminate_noise(binary, reference)
    print("eliminate sound complete")
elif selection == '6':
    res = bops.border_detection(binary, reference)
    print("detect borders complete")
else:
    res = img
io.imshow(res)
io.show()
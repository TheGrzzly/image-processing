from pip._vendor.distlib.compat import raw_input
from skimage import data, io, util
from Pymages import Filters as filt
from Pymages import BlackAndWhite as bnw
import numpy
import matplotlib.pyplot as plt

imge = io.imread('swat.png')
# imge = util.random_noise(imge, 's&p')
img = bnw.avarage(imge)
img.astype(numpy.uint8)
io.imshow(img)
imgCollection = io.image_stack
io.show()
selection = raw_input("Please select your function: ")
if selection == '1':
    res = filt.boxfilter(img)
    print("box filter complete")
elif selection == '2':
    res = filt.guassianfilter(img)
    print("gauss filter complete")
elif selection == '3':
    res = filt.mexicanhatfilter(img)
    print("mexicanhat filter complete")
elif selection == '4':
    res = filt.minfilter(img)
    print("min filter complete")
elif selection == '5':
    res = filt.maxfilter(img)
    print("max filter complete")
elif selection == '6':
    res = filt.medfilter(img)
    print("med filter complete")
else:
    res = img


io.imshow(res)
io.show()
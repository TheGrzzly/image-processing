import numpy


def scale(img, cx, cy):
    T = numpy.array([[cx, 0, 0], [0, cy, 0], [0, 0, 1]])
    y, x = img.shape
    img_d = numpy.empty((y, x), dtype=numpy.uint8)
    res = tranform(img, T, img_d)
    return res


def rotation(img, tetha):
    T = numpy.array([[numpy.cos(tetha), numpy.sin(tetha), 0], [-numpy.sin(tetha), numpy.cos(tetha), 0], [0, 0, 1]])
    y, x = img.shape
    img_d = numpy.empty((y * 2, x * 2), dtype=numpy.uint8)
    res = tranform(img, T, img_d)
    return res


def translation(img, tx, ty):
    T = numpy.array([[1, 0, tx], [0, 1, ty], [0, 0, 1]])
    y, x = img.shapeß
    img_d = numpy.empty((y * 4, x * 4), dtype=numpy.uint8)
    res = tranform(img, T, img_d)
    return res


def shearh(img, s):
    T = numpy.array([[1, s, 0], [0, 1, 0], [0, 0, 1]])
    y, x = img.shape
    img_d = numpy.empty((y * 4, x * 4), dtype=numpy.uint8)
    res = tranform(img, T, img_d)
    return res


def shearv(img, s):
    T = numpy.array([[1, 0, 0], [s, 1, 0], [0, 0, 1]])
    y, x = img.shape
    img_d = numpy.empty((y * 2, x * 2), dtype=numpy.uint8)
    res = tranform(img, T, img_d)
    return res


def tranform(img, T, img_d):
    for i, row in enumerate(img):
        for j, col in enumerate(row):
            pixel_data = img[i, j]
            input_coords = numpy.array([i, j, 1])
            i_out, j_out, _ = T @ input_coords
            img_d[int(i_out), int(j_out)] = pixel_data
    return img_d
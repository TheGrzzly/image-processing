import numpy


def boxfilter(img):
    y, x = img.shape
    img_d = numpy.empty((y, x))
    mask = numpy.array([[1/9, 1/9, 1/9], [1/9, 1/9, 1/9], [1/9, 1/9, 1/9]])
    mask2 = numpy.array([
        [1/81, 1/81, 1/81, 1/81, 1/81, 1/81, 1/81, 1/81, 1/81],
        [1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81],
        [1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81],
        [1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81],
        [1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81],
        [1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81],
        [1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81],
        [1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81],
        [1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81, 1 / 81]
    ])
    return linearfilter(img, mask, img_d).astype(numpy.uint8)


def guassianfilter(img):
    y, x = img.shape
    img_d = numpy.empty((y, x))
    mask = numpy.array([
        [0/57, 1/57, 2/57, 1/57, 0/57],
        [1/57, 3/57, 5/57, 3/57, 1/57],
        [2/57, 5/57, 9/57, 5/57, 2/57],
        [1/57, 3/57, 5/57, 3/57, 1/57],
        [0/57, 1/57, 2/57, 1/57, 0/57]
    ])
    mask2 = numpy.array([
        [1 / 81, 4 / 81, 7 / 81, 4 / 81, 1 / 81],
        [4 / 25, 16 / 81, 26 / 81, 16 / 81, 4 / 81],
        [7 / 81, 26 / 81, 41 / 81, 26 / 81, 7 / 81],
        [1 / 81, 4 / 81, 7 / 81, 4 / 81, 1 / 81],
        [4 / 25, 16 / 81, 26 / 81, 16 / 81, 4 / 81],
    ])
    return linearfilter(img, mask, img_d).astype(numpy.uint8)


def mexicanhatfilter(img):
    y, x = img.shape
    img_d = numpy.empty((y, x))
    mask = numpy.array([
        [0, 0, -1, 0, 0],
        [0, -1, -2, -1, 0],
        [-1, -2, 16, -2, -1],
        [0, -1, -2, -1, 0],
        [0, 0, -1, 0, 0]
    ])
    mask2 = numpy.array([
        [0, -1, 0],
        [-1, 4, -1],
        [0, -1, 0]
    ])
    res = linearfilter(img, mask2, img_d)
    return res.astype(numpy.uint8)


def linearfilter(img, mask, img_d):
    y, x = img.shape
    ymask, xmask = mask.shape
    half = int(xmask / 2)
    print(half)
    suma = numpy.sum(mask)
    print(suma)
    for i, row in enumerate(img):
        for j, col in enumerate(row):
            r = 0
            xs = []
            ys = []
            while r <= half:
                if i == 0 and j == 0:
                    print(float(j-r))
                if(r != 0):
                    xs.append(j+r)
                    xs.append(j-r)
                    ys.append(i+r)
                    ys.append(i-r)
                else:
                    xs.append(j)
                    ys.append(i)
                r += 1
            sum = 0;
            if i == 0 and j == 0:
                print("init value: ")
                print(sum)
                print(ys)
                print(ymask)
            for m, yvalue in enumerate(ys):
                for n, xvalue in enumerate(xs):
                    if i == 0 and j == 0:
                        print("index in y")
                        print(yvalue)
                        print("index in x")
                        print(xvalue)
                    if yvalue < 0 or yvalue>= y-1 or xvalue < 0 or xvalue >= x-1:
                        sum += 0
                        if i == 0 and j == 0:
                            print("added 0")
                            print(sum)
                    else:
                        mult = img[yvalue, xvalue] * mask[m, n]
                        sum += mult
                        if i == 0 and j == 0:
                            print("added mult")
                            print(mult)
                            print("new val")
                            print(sum)
            img_d[i, j] = sum


    return img_d


def funcfilter(img, size, func, img_d):
    y, x = img.shape
    half = int(size/2)
    for i, row in enumerate(img):
        for j, col in enumerate(row):
            r = 0
            xs = []
            ys = []
            values = []
            while r < half:
                xs.append(j + r)
                xs.append(j - r)
                ys.append(i + r)
                ys.append(i - r)
                r += 1
            for m, yvalue in enumerate(ys):
                for n, xvalue in enumerate(xs):
                    if yvalue < 0 or yvalue>= y or xvalue < 0 or xvalue >= x:
                        values.append(0)
                    else:
                        values.append(img[yvalue, xvalue])
            img_d[i, j] = func(values)
    return img_d


def minfilter(img):
    img_d = numpy.empty(img.shape)
    return funcfilter(img, 5, numpy.min, img_d)

def maxfilter(img):
    img_d = numpy.empty(img.shape)
    return funcfilter(img, 5, numpy.max, img_d)

def medfilter(img):
    img_d = numpy.empty(img.shape)
    return funcfilter(img, 5, numpy.median, img_d)

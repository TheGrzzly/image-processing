import numpy as np


def otsu(img):
    histogram, bins = np.histogram(img, np.array(range(0, 257)))
    final_variance = -1
    thresh_hold = -1
    values = []
    intensity_arr = np.arange(256)
    for level in bins:
        wa = np.sum(histogram[:level])
        wb = np.sum(histogram[level:])

        mua = np.sum(intensity_arr[:level] * histogram[:level]) / float(wa) # mu(histogram[:level], 0, wa)
        mub = np.sum(intensity_arr[level:] * histogram[level:]) / float(wb) # mu(histogram[level:], level, wb)

        va = variance(histogram[:level], 0, wa, mua)
        vb = variance(histogram[level:], level, wb, mub)
        variances = ((wa * va) + (wb * vb))
        variances = wa * wb * (mua - mub)**2
        values.append(variances)
        if variances > final_variance:
            thresh_hold = level
            final_variance = variances
    return thresh_hold, values


def mu(histogram, level, w):
    temp = 0
    for i, _ in enumerate(histogram):
        temp += (i + level) * histogram[i]
    return temp / w


def variance(histogram, level, w, mu):
    temp = 0
    for i, _ in enumerate(histogram):
        temp = ((i + level) - mu)**2 * histogram[i]
    return temp / w


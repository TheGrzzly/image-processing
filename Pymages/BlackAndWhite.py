import numpy
from skimage import data, io, util


# One channel func
def redchannel(img):
    return img[:, :, 0]


def greenchannel(img):
    return img[:, :, 1]


def bluechannel(img):
    return img[:, :, 2]


def greyshadow(img):
    convFactor = 255 / (11-1)
    avg = (img[:, :, 0] + img[:, :, 1] + img[:, :, 2])/ 3
    grey = numpy.around((((avg/convFactor)+0.5)*convFactor), decimals=0)
    return grey.astype(numpy.uint8)


def desaturation(img):
    grey = (numpy.maximum(img[:, :, 0], img[:, :, 1], img[:, :, 2]) + numpy.minimum(img[:, :, 0], img[:, :, 1], img[:, :, 2])) / 2
    return grey.astype(numpy.uint8)


def maxdecomposition(img):
    return numpy.maximum(img[:, :, 0], img[:, :, 1], img[:, :, 2])


def mindecomposition(img):
    return numpy.minimum(img[:, :, 0], img[:, :, 1], img[:, :, 2])


def avarage(img):
    grey = (img[:, :, 0] + img[:, :, 1] + img[:, :, 2]) / 3
    return grey.astype(numpy.uint8)


def lumaPhotoshop(img):
    grey = ((img[:, :, 0] * 0.3) + (img[:, :, 1] * 0.59) + (img[:, :, 2] * 0.11))
    return grey.astype(numpy.uint8)


def lumaBT709(img):
    grey = ((img[:, :, 0] * 0.2126) + (img[:, :, 1] * 0.7152) + (img[:, :, 2] * 0.0722))
    return grey.astype(numpy.uint8)

def lumaBT601(img):
    grey = ((img[:, :, 0] * 0.299) + (img[:, :, 1] * 0.587) + (img[:, :, 2] * 0.114))
    return grey.astype(numpy.uint8)

import numpy


def transform_reference(reference):
    reference_list = []
    half = int(reference.shape[0] / 2) + 1
    for y, row in enumerate(reference):
        for x, val in enumerate(row):
            if val:
                reference_list.append([(y+1 - half), (x+1 - half)])
    return reference_list


def dilate(img, reference):
    ymax, xmax = img.shape
    image_result = numpy.zeros(img.shape, dtype=numpy.bool_)
    ref_list = transform_reference(reference)
    for y, row in enumerate(img):
        for x, val in enumerate(row):
            flag = False
            for pair in ref_list:
                print("Inside ref list iter")
                if 0 <= y+pair[0] < ymax - 1 and 0 <= x + pair[1] < xmax - 1:
                    if img[y+pair[0], x + pair[1]]:
                        flag = True
                        break
            if flag:
                image_result[y, x] = True
    return image_result


def erode(img, reference):
    ymax, xmax = img.shape
    image_result = numpy.zeros(img.shape, dtype=numpy.bool_)
    ref_list = transform_reference(reference)
    for y, row in enumerate(img):
        for x, val in enumerate(row):
            flag = True
            for pair in ref_list:
                print("Inside ref list iter")
                if 0 <= y+pair[0] < ymax - 1 and 0 <= x + pair[1] < xmax - 1:
                    if not img[y + pair[0], x + pair[1]]:
                        flag = False
                        break
                else:
                    flag = False
                    break
            if flag:
                image_result[y, x] = True
    return image_result


def opening(img, reference):
    img_temp = erode(img, reference)
    img_result = dilate(img_temp, reference)
    return img_result


def lock(img, reference):
    img_temp = dilate(img, reference)
    img_result = erode(img_temp, reference)
    return img_result


def border_detection(img, reference):
    img_dilated = dilate(img, reference)
    img_eroded = erode(img, reference)
    img_result = numpy.logical_xor(img_dilated, img_eroded)
    return img_result


def eliminate_noise(img, reference):
    img_noise_open = opening(img, reference)
    img_result = lock(img_noise_open, reference)
    return img_result

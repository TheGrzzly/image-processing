import numpy


def histogram(img):
    vector = numpy.zeros(256)
    for i in img:
        for j in i:
            val = j
            vector[val] += 1
    print(vector)
    return vector


def histogramacumulated(img):
    vector = histogram(img)
    for i, pos in enumerate(vector):
        if i != 0:
            vector[i] += vector[i-1]
    print(vector)
    return vector

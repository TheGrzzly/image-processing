import numpy


def extactcolor(img, cape):
    res = img.copy()
    if cape == '0':
        res[:, :, 1].fill(0)
        res[:, :, 2].fill(0)
    elif cape == '1':
        res[:, :, 0].fill(0)
        res[:, :, 2].fill(0)
    elif cape == '2':
        res[:, :, 0].fill(0)
        res[:, :, 1].fill(0)
    return res

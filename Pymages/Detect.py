from scipy import signal
import numpy
from skimage.filters import threshold_otsu


def border_detection(image):
    x_filter = numpy.array(
        [[-1, 0, 1],
         [-1, 0, 1],
         [-1, 0, 1]])
    y_filter = numpy.array(
        [[-1, -1, -1],
         [0, 0, 0],
         [1, 1, 1]])
    x_matrix = signal.convolve2d(image, x_filter, mode="full", boundary="symm")
    y_matrix = signal.convolve2d(image, y_filter, mode="full", boundary="symm")

    y_matrix_squared = y_matrix ** 2
    x_matrix_squared = x_matrix ** 2

    y_x_matrix = y_matrix_squared + x_matrix_squared

    y_x_matrix = y_x_matrix * 0.5

    sqrted_matrix = numpy.sqrt(y_x_matrix).astype(numpy.uint8)

    thresh = threshold_otsu(sqrted_matrix).astype(numpy.uint8)
    print(thresh)

    binary = sqrted_matrix > thresh

    return binary


def corner_detection(image):
    box_filter = numpy.array(
        [[1/9, 1/9, 1/9],
         [1/9, 1/9, 1/9],
         [1/9, 1/9, 1/9]])
    x_filter = numpy.array(
        [[-1, 0, 1],
         [-1, 0, 1],
         [-1, 0, 1]]) * 0.5
    y_filter = numpy.array(
        [[-1, -1, -1],
         [0, 0, 0],
         [1, 1, 1]]) * 0.5
    z_filter = numpy.array(
        [[0, 1, 2, 1, 0],
         [1, 3, 5, 3, 1],
         [2, 5, 9, 5, 3],
         [1, 3, 5, 3, 1],
         [0, 1, 2, 1, 0]])

    alpha = 0.1
    threshold = 900

    boxed_img = signal.convolve2d(image, box_filter, mode="full", boundary="symm")

    x_matrix = signal.convolve2d(boxed_img, x_filter, mode="full", boundary="symm")
    y_matrix = signal.convolve2d(boxed_img, y_filter, mode="full", boundary="symm")

    he_11 = x_matrix ** 2
    he_22 = y_matrix ** 2
    he_12 = x_matrix * y_matrix

    a = signal.convolve2d(he_11, z_filter, mode="full", boundary="symm") * (1/57)
    b = signal.convolve2d(he_22, z_filter, mode="full", boundary="symm") * (1/57)
    c = signal.convolve2d(he_12, z_filter, mode="full", boundary="symm") * (1/57)

    v = ((a * b) - (c ** 2)) - (alpha * ((a + b)**2))

    result = list(map(lambda arr: list(map(threshcheck, arr)), v))

    res = numpy.array(result)
    res2 = max_filter(res)

    res3 = list(map(lambda arr: list(map(lastcheck, arr)), res2))
    res4 = numpy.array(res3)
    return res4


def threshcheck(n):
    r = n if n >= 900 else 0
    return r


def lastcheck(n):
    r = 255 if n != 0 else n
    return r


def funcfilter(img, size, func, img_d):
    y, x = img.shape
    half = int(size/2)
    for i, row in enumerate(img):
        for j, col in enumerate(row):
            r = 0
            xs = []
            ys = []
            values = []
            while r < half:
                xs.append(j + r)
                xs.append(j - r)
                ys.append(i + r)
                ys.append(i - r)
                r += 1
            for m, yvalue in enumerate(ys):
                for n, xvalue in enumerate(xs):
                    if yvalue < 0 or yvalue>= y or xvalue < 0 or xvalue >= x:
                        values.append(0)
                    else:
                        values.append(img[yvalue, xvalue])
            img_d[i, j] = col if func(values) == col else 0
    return img_d


def max_filter(img):
    img_d = numpy.empty(img.shape)
    return funcfilter(img, 5, numpy.max, img_d)

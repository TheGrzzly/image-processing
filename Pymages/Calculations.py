import numpy
from Pymages import Hist as h


def dynamic(img):
    return numpy.count_nonzero(numpy.unique(img))


def contrast(img):
    return numpy.amax(img) - numpy.amin(img)


def illumination(img):
    x, y = img.shape
    histogram = h.histogram(img)
    sum = 0
    for i, val in enumerate(histogram):
        sum += i * val
    return sum / (x * y)


def adaptation(img):
    max = numpy.amax(img)
    min = numpy.amin(img)
    high = 255
    low = 0
    larr = img.tolist()
    rl = list(map(lambda arr: list(map(lambda p: (p-low)*((max-min)/(high-low))+min, arr)), larr))
    res = numpy.array(rl)
    return res.astype(numpy.uint8)


def equalization(img):
    histogram = h.histogramacumulated(img)
    x, y = img.shape
    L = 45
    larr = img.tolist()
    rl = list(map(lambda arr: list(map(lambda p: histogram[p] * ((L-1)/(x * y)), arr)), larr))
    res = numpy.array(rl)
    return res.astype(numpy.uint8)

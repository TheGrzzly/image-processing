import numpy
from skimage import data, io, util
from itertools import repeat


def piexlcomplement(n):
    r = list(map(lambda p: 255 - p, n))
    return r


def brillcomplement(p, num):
    res = p + num
    if res < 0:
        res = 0
    if res > 255:
        res = 255
    return res


def contrastcomplement(p, num):
    res = p * num
    if res < 0:
        res = 0
    if res > 255:
        res = 255
    return int(res)


def complement(img):
    dim = img.ndim
    print(dim)
    if dim == 2:
        larr = img.tolist()
        copy = list(map(piexlcomplement, larr))
        copy2 = numpy.array(copy)
        return copy2.astype(numpy.uint8)
    else:
        larr = img.tolist()
        copy = list(map(lambda arr: list(map(piexlcomplement, arr)), larr))
        copy2 = numpy.array(copy)
        copy2[:, :, copy2.ndim].fill(255)
        return copy2.astype(numpy.uint8)


def brilliance(img, num):
    dim = img.ndim
    print(dim)
    if dim == 2:
        larr = img.tolist()
        rl = list(map(lambda arr: list(map(brillcomplement, arr, repeat(num, len(arr)))), larr))
        res = numpy.array(rl)
        return res.astype(numpy.uint8)
    else:
        larr = img.tolist()
        rl = list(map(lambda arr: list(map(lambda p: list(map(brillcomplement, p, repeat(num, len(p)))), arr)), larr))
        res = numpy.array(rl)
        res[:, :, res.ndim].fill(255)
        return res.astype(numpy.uint8)


def contrast(img, num):
    dim = img.ndim
    print(dim)
    if dim == 2:
        larr = img.tolist()
        rl = list(map(lambda arr: list(map(contrastcomplement, arr, repeat(num, len(arr)))), larr))
        res = numpy.array(rl)
        return res.astype(numpy.uint8)
    else:
        larr = img.tolist()
        rl = list(map(lambda arr: list(map(lambda p: list(map(contrastcomplement, p, repeat(num, len(p)))), arr)), larr))
        res = numpy.array(rl)
        res[:, :, 3].fill(255)
        return res.astype(numpy.uint8)

from skimage import data, io, util
from Pymages import BlackAndWhite as bnw
from Pymages import Detect as dt

imge = io.imread('chavab.jpg')
img = bnw.lumaBT709(imge)
io.imshow(img)
io.show()

imge2 = io.imread('figuras_gris.jpg')

border = dt.border_detection(img)
io.imshow(border)
io.show()

io.imshow(imge2)
io.show()

corners = dt.corner_detection(imge2)
io.imshow(corners)
io.show()